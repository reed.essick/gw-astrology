Note, because the PE samples from O1+O2 do not have cartesian spin components, we neglect the spin entirely when we reweigh the samples.
We also do not use sensitivity estimates (because they are not available for O1+O2).
