#!/usr/bin/env python

"""a quick script to generate fake data including single-event uncertainty and selection effects
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os
import sys

import healpy
import h5py

from argparse import ArgumentParser

### non-standard libraries
from gwdistributions import gwdio
from gwdistributions import parse as gwdist_parse
from gwdistributions import transforms
from gwdistributions.backends import numerics as be

from gwdetectors import parse as gwdet_parse

#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

parser.add_argument('config', type=str,
    help='a configuration file consistent with gw-distributions and gw-detectors')

parser.add_argument('--snr-threshold', default=8, type=float,
    help='threshold on observed snr used to simulate detection')
parser.add_argument('--approximant', default='TaylorF2', type=str)

parser.add_argument('-n', '--num-events', default=1, type=int,
    help='the number of detections to generate')
parser.add_argument('-N', '--num-catalogs', default=1, type=int,
    help='the number of catalogs to generate')

parser.add_argument('--nside', default=128, type=int)
parser.add_argument('--seed', default=None, type=int)

parser.add_argument('-v', '--verbose', default=False, action='store_true')
parser.add_argument('-V', '--Verbose', default=False, action='store_true')
parser.add_argument('--VVerbose', default=False, action='store_true')

parser.add_argument('-o', '--output-dir', default='.', type=str)
parser.add_argument('-t', '--tag', default='', type=str)

args = parser.parse_args()

args.Verbose |= args.VVerbose
args.verbose |= args.Verbose

if not os.path.exists(args.output_dir):
    os.makedirs(args.output_dir)

if args.tag:
    args.tag = "_"+args.tag

#-------------------------------------------------

be.use('numpy')

if args.seed is not None:
    if args.verbose:
        print('setting seed=%d'%args.seed)
    be.seed(args.seed)

#-------------------------------------------------

if args.verbose:
    print('parsing detector network from: '+args.config)
network = gwdet_parse(args.config, verbose=args.Verbose)

detectors = [detector.name for detector in network.detectors]

snr_names = [transforms.ObservedSNRGivenOptimalSNR._obs_snr_phs_name(name) for name in detectors]
toa_names = [transforms.TimeOfArrival._dt_name(name) for name in detectors]

#------------------------

if args.verbose:
    print('parsing distribution from: '+args.config)
generator = gwdist_parse.parse(args.config, verbose=args.Verbose)

#------------------------

if args.verbose:
    print('appending SNR transforms with approximant=%s'%args.approximant)
generator.append_transform(transforms.OptimalSNR(network=network, approximant=args.approximant))
generator.append_transform(transforms.ObservedSNRGivenOptimalSNR(detectors=detectors))

if args.verbose:
    print('appending time-of-arrival transform')
generator.append_transform(transforms.TimeOfArrival(network=network))

#------------------------

key = transforms.ObservedSNRGivenOptimalSNR._obs_snr_phs_name('net')
if args.verbose:
    print('appending conditional for detection with %s >= %.3f'%(key, args.snr_threshold))
generator.append_conditional(lambda event: event[key] >= args.snr_threshold)

#-------------------------------------------------

if args.verbose:
    print('instantiating healpix grid with nside=%d'%args.nside)
npix = healpy.nside2npix(args.nside)
dec, ra = healpy.pix2ang(args.nside, be.arange(npix))
dec = 0.5*be.pi - dec

#-------------------------------------------------

if args.verbose:
    print('generating %d mock catalogs, each with %d events'% \
        (args.num_catalogs, args.num_events))

tmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+args.tag+'_catalog-%06d.hdf5',
)

for cat in range(args.num_catalogs):
    if args.Verbose:
        print('    catalog %6d / %6d'%(cat, args.num_catalogs))

    # generate events
    generator.flush_events()
    events = generator.generate(n_event=args.num_events, verbose=args.VVerbose)

    # write catalog to disk
    path = tmp % cat
    if args.Verbose:
        print('    writing catalog to : '+path)

    with h5py.File(path, 'w') as obj:

        # simulate posteriors for each event
        grp = obj.create_group('events')
        for ind, event in enumerate(events):
            if args.Verbose:
                sys.stdout.write('\r    simulating posteriors for event %6d / %6d'%(ind, args.num_events))
                sys.stdout.flush()

            m1 = be.zeros(npix, dtype=float)
            m2 = be.zeros(npix, dtype=float)
            vr = 0.0

            group = grp.create_group('event-%06d'%ind)

            for detector, snr_name, toa_name in zip(network.detectors, snr_names, toa_names):

                # compute uncertainty in time-of-arrival
                snr = event[snr_name]

                ### FIXME?
                # compute signal bandwidth in each detector
                # estimate uncertainty in time-of-arrival in each detector
                sigma_toa = 0.0002 * (8./snr) ### based very roughly on https://arxiv.org/pdf/1010.6192.pdf
                sigma_toa = 0.001 * (8./snr) ### based very roughly on https://arxiv.org/pdf/1010.6192.pdf

                # extract true time of arrival and scatter by uncertainty
                true_geo = event['geocenter_time']

                true_toa = event[toa_name]
                obs_toa = be.normal(loc=true_toa, scale=sigma_toa)

                # add contribution to moments that we use to compute logpost (marg'd over geocent time)
                # only store the part that depends on sky position
                dt = detector.dt(true_geo, ra, dec, coord='celestial')

                m1 += (true_geo + dt - obs_toa) / sigma_toa**2
                m2 += (true_geo + dt - obs_toa)**2 / sigma_toa**2
                vr += sigma_toa**-2

                group.attrs.create('%s_toa_true'%detector.name, true_toa)
                group.attrs.create('%s_toa_obs'%detector.name, obs_toa)
                group.attrs.create('%s_toa_sigma'%detector.name, sigma_toa)

            logpost = 0.5 * m1**2 / vr - 0.5 * m2

            group.create_dataset('logpost', data=logpost)

    if args.Verbose:
        sys.stdout.write('\n')
        sys.stdout.flush()
