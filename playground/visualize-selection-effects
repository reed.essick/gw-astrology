#!/usr/bin/env python

"""a quick script to visualize selection effects for constellations
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os

import h5py

import numpy as np

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection

from argparse import ArgumentParser

### non-standard libraries
from gwdistributions import distributions

#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

parser.add_argument('injections', type=str)

parser.add_argument('-v', '--verbose', default=False, action='store_true')

parser.add_argument('-o', '--output-dir', default='.', type=str)
parser.add_argument('-t', '--tag', default='', type=str)

args = parser.parse_args()

os.makedirs(args.output_dir, exist_ok=True)

if args.tag:
    args.tag = "_"+args.tag

#-------------------------------------------------

### load injections

if args.verbose:
    print('loading injections from : '+args.injections)

with h5py.File(args.injections, 'r') as obj:
    grp = obj['injections']
    total_generated = grp.attrs['total_generated']
    injections = grp['injections'][:]

#------------------------

if args.verbose:
    print('instantiating constellation mixture')
mix = distributions.IAUConstellationMixture(num_points=1000)
#mix = distributions.HealpixConstellationMixture(nside=4)

#------------------------

### make figure
if args.verbose:
    print('plotting')

fig = plt.figure(figsize=(8,4))

ax = fig.add_axes([0.1, 0.1, 0.6, 0.6], projection='mollweide')
ax_ra = fig.add_axes([0.1, 0.75, 0.6, 0.2])
ax_dec = fig.add_axes([0.75, 0.1, 0.2, 0.6])

#injections = injections[injections['snr_net'] > 20]

nbins = max(10, int(0.5*len(injections)**0.5))

ra = np.where(injections['right_ascension'] > np.pi, injections['right_ascension']-2*np.pi, injections['right_ascension'])
dec = injections['declination']

ax.plot(
    ra,
    dec,
    markeredgecolor='none',
    markerfacecolor='k',
    marker='o',
    markersize=2,
    linestyle='none',
    alpha=0.25,
)

ax_ra.hist(ra, bins=np.linspace(-np.pi, +np.pi, nbins), histtype='step', orientation='vertical', alpha=0.50, color='orange', density=True)
ax_ra.hist(ra, bins=np.linspace(-np.pi, +np.pi, nbins), histtype='stepfilled', orientation='vertical', alpha=0.25, color='orange', density=True)

ax_dec.hist(dec, bins=np.linspace(-np.pi/2, +np.pi/2, nbins), histtype='step', orientation='horizontal', alpha=0.50, color='orange', density=True)
ax_dec.hist(dec, bins=np.linspace(-np.pi/2, +np.pi/2, nbins), histtype='stepfilled', orientation='horizontal', alpha=0.25, color='orange', density=True)

ax_ra.set_xlim(xmin=-np.pi, xmax=+np.pi)
ax_ra.set_xticks(ax.get_xticks())

ax_dec.set_ylim(ymin=-np.pi/2, ymax=+np.pi/2)
ax_dec.set_yticks(ax.get_yticks())

# plot the isotropic distribution of dec
dec = np.linspace(-0.5*np.pi, +0.5*np.pi, 101)
ax_dec.plot(0.5*np.cos(dec), dec, color='k', alpha=0.50)

ra = np.linspace(-np.pi, +np.pi, 101)
ax_ra.plot(ra, np.ones_like(ra)/(2*np.pi), color='k', alpha=0.50)

# plot the constellations
patches = []

exposure = []
for dist in mix.distributions:
    exposure.append(np.sum(dist.prob(injections['right_ascension'], injections['declination']) / (0.5 * np.cos(injections['declination']))))
exposure = np.array(exposure)
exposure /= np.max(exposure)

for exp, dist in zip(exposure, mix.distributions):
    
    for shift in [0, 2*np.pi]:
        for (ra, dec), (RA, DEC) in dist.edges:
            ax.plot([ra-shift, RA-shift], [dec, DEC], color='k', alpha=0.25)

        for ra, dec in dist.vertices:
            ax.plot(ra-shift, dec, marker='.', markersize=1, color='k', alpha=1.0)

        verts = np.transpose([dist.vertices[:,0]-shift, dist.vertices[:,1]])
#        patches.append(Polygon(verts, closed=True, alpha=0.15, edgecolor='red', facecolor='orange'))
        ax.add_patch(Polygon(verts, closed=True, alpha=exp, edgecolor='red', facecolor='orange'))

#ax.add_collection(PatchCollection(patches))

ax.grid(True, which='both')

for a in [ax, ax_ra, ax_dec]:
    plt.setp(a.get_xticklabels(), visible=False)
    plt.setp(a.get_yticklabels(), visible=False)
    a.tick_params(
        left=True,
        right=True,
        top=True,
        bottom=True,
        direction='in',
        which='both',
    )

path = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+args.tag+'.png'
)
if args.verbose:
    print('    saving : '+path)
fig.savefig(path)
plt.close(fig)
