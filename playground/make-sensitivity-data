#!/usr/bin/env python

"""a quick script to generate fake data including single-event uncertainty and selection effects
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os
import sys

import healpy
import h5py

from argparse import ArgumentParser

### non-standard libraries
from gwdistributions import gwdio
from gwdistributions import parse as gwdist_parse
from gwdistributions import transforms
from gwdistributions.backends import numerics as be

from gwdetectors import parse as gwdet_parse

#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

parser.add_argument('config', type=str,
    help='a configuration file consistent with gw-distributions and gw-detectors')

parser.add_argument('--snr-threshold', default=12, type=float,
    help='threshold on observed snr used to simulate detection')
parser.add_argument('--approximant', default='TaylorF2', type=str)

parser.add_argument('-m', '--num-injections', default=1000, type=int,
    help='the number of injections used to estimate survey sensitivity')

parser.add_argument('--seed', default=None, type=int)

parser.add_argument('-v', '--verbose', default=False, action='store_true')
parser.add_argument('-V', '--Verbose', default=False, action='store_true')

parser.add_argument('-o', '--output-dir', default='.', type=str)
parser.add_argument('-t', '--tag', default='', type=str)

args = parser.parse_args()

args.verbose |= args.Verbose

if not os.path.exists(args.output_dir):
    os.makedirs(args.output_dir)

if args.tag:
    args.tag = "_"+args.tag

#-------------------------------------------------

be.use('numpy')

if args.seed is not None:
    if args.verbose:
        print('setting seed=%d'%args.seed)
    be.seed(args.seed)

#-------------------------------------------------

if args.verbose:
    print('parsing detector network from: '+args.config)
network = gwdet_parse(args.config, verbose=args.Verbose)

detectors = [detector.name for detector in network.detectors]

snr_names = [transforms.ObservedSNRGivenOptimalSNR._obs_snr_phs_name(name) for name in detectors]
toa_names = [transforms.TimeOfArrival._dt_name(name) for name in detectors]

#------------------------

if args.verbose:
    print('parsing distribution from: '+args.config)
generator = gwdist_parse.parse(args.config, verbose=args.Verbose, store_logprob=False, store_factored_logprob=True)

#------------------------

if args.verbose:
    print('appending SNR transforms with approximant=%s'%args.approximant)
generator.append_transform(transforms.OptimalSNR(network=network, approximant=args.approximant))
generator.append_transform(transforms.ObservedSNRGivenOptimalSNR(detectors=detectors))

if args.verbose:
    print('appending time-of-arrival transform')
generator.append_transform(transforms.TimeOfArrival(network=network))

#------------------------

#key = transforms.ObservedSNRGivenOptimalSNR._obs_snr_phs_name('net')
key = 'snr_net'
if args.verbose:
    print('appending conditional for detection with %s >= %.3f'%(key, args.snr_threshold))
generator.append_conditional(lambda event: event[key] >= args.snr_threshold)

#-------------------------------------------------

if args.verbose:
    print('generating %d injections'% args.num_injections)

path = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+args.tag+'.hdf5',
)

# generate injections
generator.flush_events()
injections = generator.generate(n_event=args.num_injections, verbose=args.Verbose)
total_generated = generator.total_generated

path = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+args.tag+'.hdf5',
)
if args.verbose:
    print('saving : '+path)
with h5py.File(path, 'w') as obj:

    # save injections
    grp = obj.create_group('injections')
    grp.attrs.create('total_generated', total_generated)
    grp.create_dataset('injections', data=gwdio.events2array(injections, generator.attributes))
