# gw-astrology

Proposal:

We will attempt to perform a "tomographic" measurement of the rate density of merging systems. As a first attempt, we will assume the mass distribution does not depend on the redshift, RA, or Dec.

That is, in general we can write
```
dN ~ R(RA, Dec) * p(z|RA, Dec) * p(m1, m2, a1, a2, ...|z, RA, Dec)
```
and we will approximate this as
```
dN ~ R(RA, Dec) * p(z|RA, Dec) * p(m1, m2, a1, a2, ...)
```

With this general assumption, we may consider several models

  * `R(RA, Dec)` is piece-wise constant within pixels
    - healpix decomposition or IAU constellations
    - hyperprior for `R(RA, Dec)` may correlate nearby rates to control how quickly they can change across the sky
  * `R(RA, Dec)` is described by a spherical harmonic decomposition
    - hyperprior can probably be independent for different harmonic numbers

and

  * `p(z|RA, Dec)` is described by a binned histogram (and/or full Gaussian process)
    - hyperprior may correlate bins that are nearby in both `z` and `RA, Dec`
  * `p(z|RA, Dec)` is described parametrically with something like `(1+z)**k * (dVc/dz) * (1+z)**-1`
    - hyperprior may correlate parameters (`k`) that belong to pixels that are nearby in `RA, Dec`

---

Dependencies:

  * [gw-distributions](https://git.ligo.org/reed.essick/gw-distributions)
  * [numpyro](https://num.pyro.ai/en/latest/index.html)

---

initial project ideas:

  * fit rates for different constellations (assuming same mass, spin, redshift for all constellations)
    - show results for each zodiac (including non-western zodiacs?)
    - should rates in constellations bet independent a priori or correlated in some way based on a notion of their separation
  * fit separate mass, redshift, spin distributions for each constellations
    - build a model for `(mass, spin|redshift)`
      * use separate distributions for each pixel
      * build a tomographical map of source distribution
  * compute single-event probability of being in each constellations
    - assign each event a zodiac
  * check whether Virgo is more or less likely to detect sources from Virgo (the constellation)
  * estimate selection effects with real/time-dependent PSDs
  * look for correlations between "mass" and clustering scales
  * estimate how many events you need before you can resolve structure meaningfully with GWs alone
  * model comparison between cross-correlation with AGN or galaxy catalog and "independent sources"
    - CMB and/or SZ surveys
  * use constellations as tests of model dependence from choice of pixels?

---

## Paper proposal

### Isotropy

  * introduce formalism
  * show fits with different pixelizations
    - pixelizations:
      * hemisphere pixelization
      * healpix nside = 1, 2, 4
      * iau constellations
      * galactic bluge, disk + nearby objects (LMC, SMC, Andromeda, etc) + extragalactice (isotropic)?
    - constriants on variance are weaker when we have more (smaller) pixels
    - preference for isotropy is mild at best
  * show fits with spherical harmonics
    - demonstrate that only the low-l modes are constrained by current data
  * run through "perturbation theory" for small anisotropy field and analyze the GP derrived therefrom
    - distribution of eigenvalues of the likelihood, scaling with catalog size
      - show this in pixel-domain and spherical-harmonic-domain
    - analyze behavior with catalogs of many (simulated) skymaps and the likelihood from the current set of skymaps

### Homogeneity

  * discussion of when/how we can perform these measurements
    - CBCs come with an estimate of luminosity distance
    - we can estimate the distance for burst sources if we assume a (distrib of) energy scales (in source-frame)
  * perform basic fit with "hemisphere" pixelization : params are just the pole defining the hemispheres (2 params)
    - `p(m1, m2, z|RA, Dec)` in separate hemispheres
  * else?
