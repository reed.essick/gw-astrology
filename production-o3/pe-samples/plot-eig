#!/usr/bin/env python3

"""a script to plot the results of the eigenvalue analysis
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os

import h5py
import numpy as np

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

from matplotlib import patheffects
from matplotlib import text

from argparse import ArgumentParser

#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

parser.add_argument('hdf', type=str)

parser.add_argument('--color-map', default='Reds', type=str)

parser.add_argument('-v', '--verbose', default=False, action='store_true')

parser.add_argument('-o', '--output-dir', default='.', type=str)
parser.add_argument('-t', '--tag', default='', type=str)
parser.add_argument('--figtype', default=[], type=str, action='append')
parser.add_argument('--dpi', default=100, type=float)

args = parser.parse_args()

os.makedirs(args.output_dir, exist_ok=True)

if args.tag:
    args.tag = "_"+args.tag

if not args.figtype:
    args.figtype.append('png')

#-------------------------------------------------

if args.verbose:
    print('loading : '+args.hdf)
with h5py.File(args.hdf, 'r') as obj:
    ra = obj['ra'][:]
    dec = obj['dec'][:]
    eigvals = obj['eigvals'][:]
    eigvecs = obj['eigvecs'][:]
    inv_cov = obj['inv_cov'][:]

# change the ordering of ra, dec, and eigvecs
n = len(ra) // 2
ra = np.concatenate((ra[n:]-2*np.pi, ra[:n]))
dec = np.concatenate((dec[n:], dec[:n]))
for ind in range(len(eigvecs)):
    eigvecs[:,ind] = np.concatenate((eigvecs[n:,ind], eigvecs[:n,ind]))

#------------------------

# figure out when to cut off eigvals
shape = (int(len(eigvals)**0.5),)*2

num = int(np.sum(eigvals >= 1e-6))

#------------------------

if args.verbose:
    print('plotting distribution of eigenvalues')

fig = plt.figure(figsize=(7,3))
ax = fig.add_axes([0.10, 0.15, 0.85, 0.40])

# add distribution of eigenvalues
ax.plot(1 + np.arange(len(eigvals)), eigvals, marker='.')

ax.set_xlabel('eigenvalue index')
ax.set_ylabel('eigenvalue')

ax.set_xlim(xmin=0.0, xmax=num+1)
ax.set_xticks([1, 10, 20, 30, 40, 50, 60, 63])

ax.set_yscale('log')
ax.set_ylim(ymin=1e1, ymax=1.5e5)
ax.set_yticks([1e1, 1e2, 1e3, 1e4, 1e5])

# add examples of eigenvectors

### best constrained eigenvector
ax1 = fig.add_axes([0.005, 0.575, 0.32, 0.38], projection='mollweide')

ind = 0 

ax.text(1+ind, eigvals[ind], 'A', ha='center', va='top')
ax1.text(0, np.pi/2, 'A', ha='center', va='bottom')

ax1.pcolormesh(
    ra.reshape(shape),
    dec.reshape(shape),
    eigvecs[:,ind].reshape(shape),
    rasterized=True,
    cmap=args.color_map,
)

### moderately constrained eigenvector
ax2 = fig.add_axes([0.335, 0.575, 0.32, 0.38], projection='mollweide')

ind = num//3 + 2

ax.text(1+ind, eigvals[ind], 'B', ha='center', va='bottom')
ax2.text(0, np.pi/2, 'B', ha='center', va='bottom')

ax2.pcolormesh(
    ra.reshape(shape),
    dec.reshape(shape),
    eigvecs[:,ind].reshape(shape),
    rasterized=True,
    cmap=args.color_map,
)

### worst constrained eigenvector
ax3 = fig.add_axes([0.665, 0.575, 0.32, 0.38], projection='mollweide')

ind = 2*num//3 + 2

ax.text(1+ind, eigvals[ind], 'C', ha='center', va='bottom')
ax3.text(0, np.pi/2, 'C', ha='center', va='bottom')

ax3.pcolormesh(
    ra.reshape(shape),
    dec.reshape(shape),
    eigvecs[:,ind].reshape(shape),
    rasterized=True,
    cmap=args.color_map,
)

# finish decorating

#fig.patch.set_alpha(0.)

ax.tick_params(
    left=True,
    right=True,
    top=True,
    bottom=True,
    direction='in',
    which='both',
)

ax.patch.set_alpha(0.)
ax.set_alpha(0.)

for a in [ax1, ax2, ax3]:
    a.grid(True)

    a.tick_params(
        left=False,
        right=False,
        top=False,
        bottom=False,
        direction='in',
        which='both',
    )
    a.patch.set_alpha(0.)
    a.set_alpha(0.)

    plt.setp(a.get_xticklabels(), visible=False)
    plt.setp(a.get_yticklabels(), visible=False)

    effects = [patheffects.withStroke(linewidth=2, foreground='w')]
    for artist in a.findobj(text.Text):
        artist.set_path_effects(effects)


# save
tmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__) + ('-eigvals%s'%args.tag) + ".%s"
)
for figtype in args.figtype:
    path = tmp % figtype
    if args.verbose:
        print('saving : '+path)
    fig.savefig(path, dpi=args.dpi)
plt.close(fig)
