#!/usr/bin/env python

"""a quick script to precompute the terms needed to assemble the likelihood for a fit to the IAUConstellationMixture
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os
import sys

import healpy as hp ### specialized to the HealpixConstellationMixture

import h5py
import numpy as np ### for io

from argparse import ArgumentParser

### non-standard libraries
from gwdistributions import (parse, distributions)
from gwdistributions.backends import numerics as be
from gwdistributions.backends import names

#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

parser.add_argument('fixed_config', type=str)
parser.add_argument('sensitivity_estimate', type=str)
parser.add_argument('events', nargs='+', type=str)

parser.add_argument('--max-far', default=1, type=float,
    help='the maximum allowed FAR (detection threshold). Specified in 1/yr')
parser.add_argument('--nside', default=1, type=int)

parser.add_argument('-v', '--verbose', default=False, action='store_true')
parser.add_argument('-V', '--Verbose', default=False, action='store_true')
parser.add_argument('--VVerbose', default=False, action='store_true')

parser.add_argument('-o', '--output-dir', default='.', type=str)
parser.add_argument('-t', '--tag', default='', type=str)

args = parser.parse_args()

args.Verbose |= args.VVerbose
args.verbose |= args.Verbose

os.makedirs(args.output_dir, exist_ok=True)

if args.tag:
    args.tag = "_"+args.tag

#-------------------------------------------------

be.use('numpy', verbose=args.VVerbose)
names.use('redshift', 'redshift')

#-------------------------------------------------

if args.verbose:
    print('instantiating (fixed) mass, redshift, and spin distributions')

model = parse.parse(args.fixed_config, verbose=args.Verbose)

#------------------------

if args.verbose:
    print('instantiating HealpixConstellationMixture with nside=%d'%args.nside)
mix = distributions.HealpixConstellationMixture(nside=args.nside)
npix = mix.num_components

#------------------------

path = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+args.tag+'.hdf5',
)
if args.verbose:
    print('writing : '+path)

### compute and store data
with h5py.File(path, 'w') as obj:

    # store some data
    if args.verbose:
        print('storing meta-data for mixture model')

    dtype = [
        ('name', 'S50'),
        ('area', float),
        ('param_name', 'S50'),
        ('ra', float),
        ('dec', float),
    ]
    data = np.empty(mix.num_components, dtype=dtype)

    theta, ra = hp.pix2ang(args.nside, np.arange(mix.num_components))
    data['ra'][:] = ra
    data['dec'][:] = 0.5*np.pi - theta

    for ind, dist in enumerate(mix.distributions):
        data['name'][ind] = str(ind)
        data['area'][ind] = dist.area
        data['param_name'][ind] = 'mixture_frac%d'%ind

    obj.create_dataset('mixture_params', data=data)

    #---

    # pre-compute sensitivity estimate terms
    if args.verbose:
        print('loading injections from : '+args.sensitivity_estimate)

    grp = obj.create_group('sensitivity_estimate')
    grp.attrs.create('path', args.sensitivity_estimate)

    data = np.genfromtxt(args.sensitivity_estimate, names=True, delimiter=',')
    if args.verbose:
        print('    found %d samples'%len(data))

    found = data['far'] <= args.max_far
    data = data[found]
    if args.verbose:
        print('    retained %d samples with FAR <= %.3e'%(len(data), args.max_far))

    # compute the weights that are common regardless of constellation
    weights = model.prob(data)
    weights /= np.exp(data['lnprob_mass1_source_mass2_source_redshift_spin1x_spin1y_spin1z_spin2x_spin2y_spin2z'])

    weights /= np.max(weights) ### normalization does not matter

    # iterate over constellations
    factors = []
    for pix, dist in enumerate(mix.distributions):
        if args.Verbose:
            sys.stdout.write('\r    processing : %8d / %8d'%(pix, npix))
            sys.stdout.flush()

        prob = dist.prob(data['right_ascension'], data['declination'])
        draw = np.exp(data['lnprob_right_ascension']+data['lnprob_declination'])
        factors.append( be.sum(weights*(prob/draw)) )

    if args.Verbose:
        sys.stdout.write('\r')
        sys.stdout.flush()

    factors = np.array(factors) / np.sum(factors) ### normalize because this does not affect the fit

    if args.VVerbose:
        print('    factors : ', factors)

    grp.create_dataset('sensitivity_estimate', data=factors)

    #---

    # pre-compute single-event terms
    grp = obj.create_group('events')

    for path in args.events:
        name = os.path.basename(path)[:-7]
        if args.verbose:
            print('loading parameter estimation samples for %s from : %s'%(name, path))
        grp.attrs.create(name, path)

        data = np.genfromtxt(path, names=True, delimiter=',')
        if args.verbose:
            print('    found %d samples'%len(data))

        # compute weights that are constant independent of the constellation
        weights = model.prob(data)
        weights /= np.exp(data['lnprob_mass1_source'] + data['lnprob_mass2_source'] + data['lnprob_redshift'] \
            + data['lnprob_spin1x_spin1y_spin1z'] + data['lnprob_spin2x_spin2y_spin2z'])

        weights /= np.max(weights)

        # iterate over constellations
        factors = []
        for pix, dist in enumerate(mix.distributions):
            if args.Verbose:
                sys.stdout.write('\r    processing : %8d / %8d'%(pix, npix))
                sys.stdout.flush()

            prob = dist.prob(data['right_ascension'], data['declination'])
            draw = np.exp(data['lnprob_right_ascension']+data['lnprob_declination'])
            factors.append( be.sum(weights*(prob/draw)) )

        if args.Verbose:
            sys.stdout.write('\r')
            sys.stdout.flush()

        factors = np.array(factors) / np.sum(factors) ### normalize because this does not affect the fit

        if args.VVerbose:
            print('    factors : ', factors)

        grp.create_dataset(name, data=factors)
