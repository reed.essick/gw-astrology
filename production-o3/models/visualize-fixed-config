#!/usr/bin/env python3

"""a quick script to generate visualizations of the fixed population models
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os

import numpy as np

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
#plt.rcParams['text.usetex'] = True

from argparse import ArgumentParser

### non-standard libraries
from gwdistributions import (parse, distributions)
from gwdistributions.backends import names

#-------------------------------------------------

TICK_PARAMS = dict(
    left=True,
    right=True,
    top=True,
    bottom=True,
    direction='in',
    which='both',
)

FIGSIZE = (2.5, 2.5)

SUBPLOTS_ADJUST = dict(
    left=0.25,
    right=0.93,
    bottom=0.25,
    top=0.93,
)

#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

parser.add_argument('fixed_config', type=str)

parser.add_argument('--num-grid-points', default=101, type=int)

parser.add_argument('--cmap', default='Blues', type=str)

parser.add_argument('-v', '--verbose', default=False, action='store_true')

parser.add_argument('-o', '--output-dir', default='.', type=str)
parser.add_argument('-t', '--tag', default='', type=str)
parser.add_argument('--figtype', default=[], type=str, action='append')
parser.add_argument('--dpi', default=100, type=float)

args = parser.parse_args()

os.makedirs(args.output_dir, exist_ok=True)

if args.tag:
    args.tag = "_"+args.tag

if not args.figtype:
    args.figtype.append('png')

#-------------------------------------------------

names.use('redshift', 'redshift')

#-------------------------------------------------

model = parse.parse(args.fixed_config, verbose=args.verbose)

#-------------------------------------------------

# plot redshift distribution
if args.verbose:
    print('plotting redshift distribution')

### plot p(z)
for dist in model.generators:
    if isinstance(dist, distributions.RedshiftDistribution):
        break
else:
    raise RuntimeError('could not find RedshiftDistribution')

#--- pull out relevant range of redshifts
z = np.linspace(*dist.domain()['redshift'], args.num_grid_points)

#--- actually plot
fig = plt.figure(figsize=FIGSIZE)
ax = fig.gca()

ax.plot(z, dist.prob(z))

ax.set_xlim(xmin=z[0], xmax=z[-1])
plt.setp(ax.get_yticklabels(), visible=False)

ax.set_xlabel('$z$')
ax.set_ylabel('$p(z)$')

ax.tick_params(**TICK_PARAMS)
plt.subplots_adjust(**SUBPLOTS_ADJUST)

figtmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+'-redshift'+args.tag+'.%s'
)
for figtype in args.figtype:
    figname = figtmp % figtype
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname, dpi=args.dpi)
plt.close(fig)

#-------------------------------------------------

# plot spin distribution
if args.verbose:
    print('plotting spin distribution')

for dist in model.generators:
    if isinstance(dist, distributions.Spin1Spin2Distribution):
        break
else:
    raise RuntimeError('could not find spin distribution')

### declare a distribution over just 1 spin (3-vector)
dist = distributions.IsotropicPowerLawSpin(
    a_max=dist['a1_max'],
    a_min=dist['a1_min'],
    a_pow=dist['a1_pow'],
)

spin = np.linspace(0.0, 1.0, args.num_grid_points)

SPINX, SPINY, SPINZ = np.meshgrid(spin, spin, spin, indexing='ij')
SPINX = SPINX.ravel()
SPINY = SPINY.ravel()
SPINZ = SPINZ.ravel()

### plot "spin disk"
if args.verbose:
    print('plotting spin disk')

#--- actually plot

prob = dist.prob(SPINX, SPINY, SPINZ).reshape((args.num_grid_points, args.num_grid_points, args.num_grid_points))

prob2D = np.sum(prob, axis=2)*(spin[1]-spin[0]) # marginalize over one direction
prob1D = np.sum(prob2D, axis=1)*(spin[1]-spin[0]) # marginalize over another direction

#--- 2D marginal

fig = plt.figure(figsize=FIGSIZE)
ax = fig.gca()

ax.contourf(
    spin,
    spin,
    np.log10(prob2D),
    cmap=args.cmap,
)

xmin = spin[0]
xmax = spin[-1]
dx = (xmax-xmin)*0.05

ax.text(xmax-dx, xmax-dx, '$\log_{10}(p(s_x, s_y))$', ha='right', va='top')

ax.set_xlim(xmin=xmin, xmax=xmax)
ax.set_ylim(ax.get_xlim())

ax.set_xlabel('$s_x$')
ax.set_ylabel('$s_y$')

ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])

ax.tick_params(**TICK_PARAMS)
plt.subplots_adjust(**SUBPLOTS_ADJUST)

figtmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+'-spin2D'+args.tag+'.%s'
)
for figtype in args.figtype:
    figname = figtmp % figtype
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname, dpi=args.dpi)
plt.close(fig)

#--- 1D marginal

fig = plt.figure(figsize=FIGSIZE)
ax = fig.gca()

ax.plot(spin, prob1D)

ax.set_xlabel('$s_x$')
ax.set_ylabel('$p(s_x)$')

ax.set_xlim(xmin=spin[0], xmax=spin[-1])

ax.tick_params(**TICK_PARAMS)
plt.subplots_adjust(**SUBPLOTS_ADJUST)

plt.setp(ax.get_yticklabels(), visible=False)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])

figtmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+'-spin1D'+args.tag+'.%s'
)
for figtype in args.figtype:
    figname = figtmp % figtype
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname, dpi=args.dpi)
plt.close(fig)

#-------------------------------------------------

# plot mass distribution
if args.verbose:
    print('plotting mass distribution')

for dist in model.generators:
    if isinstance(dist, distributions.ComponentMassDistribution):
        break
else:
    raise RuntimeError('could not find mass distribution')

### plot (mass1_source, mass2_source) distribution
if args.verbose:
    print('plotting 2D mass distribution')

#--- pull out relevant ranges for masses

### NOTE: hard-coded to give reasonable ranges
#mass1 = np.linspace(1.0, 50.0, args.num_grid_points)
#mass2 = np.linspace(1.0, 50.0, args.num_grid_points)
mass = np.logspace(0.0, 2.0, args.num_grid_points)

M1, M2 = np.meshgrid(mass, mass)
M1 = M1.ravel()
M2 = M2.ravel()

Z = (M1*M2*dist.prob(M1, M2)).reshape((args.num_grid_points, args.num_grid_points))
Z = np.log10(Z)

#--- actually plot

fig = plt.figure(figsize=FIGSIZE)
ax = fig.gca()

'''
m = ax.imshow(
    Z,
    aspect='auto',
    origin='lower',
    extent=[np.min(mass1), np.max(mass1), np.min(mass2), np.max(mass2)],
    cmap=args.cmap,
)
'''

m = ax.contourf(
    mass,
    mass,
    Z,
    cmap=args.cmap,
)

xmin = mass[0]
xmax = mass[-1]
dx = (xmax/xmin)**0.05

ax.text(xmin*dx, xmax/dx, '$\log_{10}(m_1 m_2 p(m_1, m_2))$', ha='left', va='top')

ax.set_xlim(xmin=xmin, xmax=xmax)
ax.set_ylim(ax.get_xlim())

ax.set_xscale('log')
ax.set_yscale('log')

ax.set_xlabel('$m_1\,[M_\odot]$')
ax.set_ylabel('$m_2\,[M_\odot]$')

ax.tick_params(**TICK_PARAMS)
plt.subplots_adjust(**SUBPLOTS_ADJUST)

figtmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+'-mass2D'+args.tag+'.%s'
)
for figtype in args.figtype:
    figname = figtmp % figtype
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname, dpi=args.dpi)
plt.close(fig)

#------------------------

### plot mass1_source distribution
if args.verbose:
    print('plotting 1D mass distribution')

#--- pull out relevant range of masses
dist = dist.massdist ### pull out the 1D mass distribution
mass1 = np.logspace(*np.log10(dist.domain()['mass1_source']), args.num_grid_points)

#--- actually plot
fig = plt.figure(figsize=FIGSIZE)
ax = fig.gca()

ax.plot(mass1, dist.prob(mass1)*mass1)

ax.set_xscale('log')

#ax.set_xlim(xmin=mass1[0], xmax=mass1[-1])
ax.set_xlim(xmin=1.0, xmax=100.0) ### NOTE: hard-coded to give reasonable ranges
plt.setp(ax.get_yticklabels(), visible=False)

ax.set_xlabel('$m\,[M_\odot]$')
ax.set_ylabel('$m \cdot p(m)$')

ax.tick_params(**TICK_PARAMS)
plt.subplots_adjust(**SUBPLOTS_ADJUST)

figtmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+'-mass1D'+args.tag+'.%s'
)
for figtype in args.figtype:
    figname = figtmp % figtype
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname, dpi=args.dpi)
plt.close(fig)
