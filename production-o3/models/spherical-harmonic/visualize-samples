#!/usr/bin/env python3

"""a script to visualize the output of the samplers
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os
import sys

import h5py
import numpy as np

import healpy as hp

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
plt.rcParams['text.usetex'] = True

import corner

from argparse import ArgumentParser

### non-standard libraries
from gwdistributions.distributions import ExponentialSphericalHarmonicRADec

#-------------------------------------------------

def complex2real4corner(samples, max_l):
    labels = []
    ans = []
    for l in range(max_l+1):
#        labels.append('$\mathcal{R}_{%d,0}/\sqrt{\sum |b_{lm}|^2}$'%l)
        labels.append('$\mathcal{R}_{%d,0}$'%l)
        ans.append(samples['b_%d_0'%l].real)
        for m in range(1, l+1):
#            labels += ['$\mathcal{R}_{%d,%d}/\sqrt{\sum |b_{lm}|^2}$'%(l,m), '$\mathcal{I}_{%d,%d}/\sqrt{\sum |b_{lm}|^2}$'%(l,m)]
            labels += ['$\mathcal{R}_{%d,%d}$'%(l,m), '$\mathcal{I}_{%d,%d}$'%(l,m)]
            val = samples['b_%d_%d'%(l,m)]
            ans += [val.real, val.imag]
    ans = np.array(ans)
#    norm = np.sum(ans**2, axis=0)**0.5
#    for i in range(len(ans)):
#        ans[i,:] /= norm
    return np.transpose(ans), labels, [np.nan] + [0.0]*(len(labels)-1)

def complex2pow4corner(samples, max_l):
    labels = []
    ans = []
    for l in range(max_l+1):
        wer = 0.0
        for m in range(l+1):
            wer += np.abs(samples['b_%d_%d'%(l,m)])**2
        ans.append(wer)
#        labels.append('$\sum_m |b_{%dm}|^2/|\sum_{lm} |b_{lm}|^2$'%l)
        labels.append('$\sum_m |b_{%dm}|^2$'%l)
    ans = np.array(ans)
#    norm = np.sum(ans, axis=0)
#    for i in range(len(ans)):
#        ans[i,:] /= norm
    return np.transpose(ans), labels, [1.0]+[0.0]*(len(labels)-1)
 
#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

parser.add_argument('hdf', type=str)
parser.add_argument('max_l', type=int)

parser.add_argument('--seed', default=None, type=int)

parser.add_argument('--nside', default=64, type=int)

parser.add_argument('--plot-mollweide', default=False, action='store_true')

parser.add_argument('--num-draws', default=20, type=int,
    help='plot this number of draws from prior, posterior')
parser.add_argument('--num-draws-for-average', default=1000, type=int,
    help='the number of draws used to compute the average of the rate over the sky')

parser.add_argument('-v', '--verbose', default=False, action='store_true')

parser.add_argument('-o', '--output-dir', default='.', type=str)
parser.add_argument('-t', '--tag', default='', type=str)
parser.add_argument('--figtype', default=[], type=str, action='append')
parser.add_argument('--dpi', default=100, type=float)

args = parser.parse_args()

os.makedirs(args.output_dir, exist_ok=True)

if args.tag:
    args.tag = "_"+args.tag

if not args.figtype:
    args.figtype.append('png')

#-------------------------------------------------

if args.seed is not None:
    if args.verbose:
        print('seeting numpy.random.seed=%d'%args.seed)
    np.random.seed(args.seed)

#-------------------------------------------------

if args.verbose:
    print('loading samples from : '+args.hdf)
with h5py.File(args.hdf, 'r') as obj:
    prior_samples = dict((key, obj['prior'][key][:]) for key in obj['prior'].keys())
    posterior_samples = dict((key, obj['posterior'][key][:]) for key in obj['posterior'].keys())

#------------------------

if args.verbose:
    print('instantiating ExponentialSphericalHarmonicRADec')

kwargs = dict()
for l in range(args.max_l+1):
    for m in range(l+1):
        kwargs['b_%d_%d'%(l,m)] = 0.0j
dist = ExponentialSphericalHarmonicRADec(**kwargs)

#-------------------------------------------------

if args.verbose:
    print('corner plot : coefficients')
fig = None

for ind, (samples, label, color) in enumerate([
        (prior_samples, 'prior', 'b'),
        (posterior_samples, 'posterior', 'r'),
    ]):
    samples, labels, truths = complex2real4corner(samples, args.max_l)

    fig = corner.corner(
        samples,
        labels=labels,
        truths=truths,
        truth_color='grey',
        color=color,
        hist_kwargs={'density':True},
        fig=fig,
    )

    fig.text(0.95, 0.95-0.05*ind, label, color=color, ha='right', va='top')

# save
figtmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+'-corner'+args.tag+'.%s'
)
for figtype in args.figtype:
    figname = figtmp % figtype
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname, dpi=args.dpi)
plt.close(fig)

#-------------------------------------------------

if args.verbose:
    print('corner plot: power in each angular harmonic')
fig = None

for ind, (samples, label, color) in enumerate([
        (prior_samples, 'prior', 'b'),
        (posterior_samples, 'posterior', 'r'),
    ]):
    samples, labels, truths = complex2pow4corner(samples, args.max_l)

    fig = corner.corner(
        samples,
        labels=labels,
        truths=truths,
        truth_color='k',
        color=color,
        hist_kwargs={'density':True},
        fig=fig,
    )

    fig.text(0.95, 0.95-0.05*ind, label, color=color, ha='right', va='top')

# save
figtmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+'-corner-l'+args.tag+'.%s'
)
for figtype in args.figtype:
    figname = figtmp % figtype
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname, dpi=args.dpi)
plt.close(fig)

#-------------------------------------------------

if args.plot_mollweide:

    if args.verbose:
        print('mollweide projections')

    # instantiate healpix arrays
    npix = hp.nside2npix(args.nside)
    pix = np.arange(npix)

    dec, ra = hp.pix2ang(args.nside, pix)
    dec = 0.5*np.pi - dec

    jac = np.cos(dec)
    jac[jac==0.0] = 1.0 ### avoid divergences...

    idv = np.empty(npix, dtype=float)
    ave = np.empty(npix, dtype=float)

    for samples, name, cmap in [
            (prior_samples, 'prior', 'Blues'),
            (posterior_samples, 'posterior', 'Reds'),
        ]:
        N = len(samples['b_0_0'])
        n = 0
#        n = N//2 # throw away roughly the first half of the samples as burn-in
#                 # the flat chains may not be indexed nicely like this, though...

        KEYS = [key for key in samples.keys() if key[:2]=='b_']
        LABELS = dict()
        for key in KEYS:
            l, m = [int(_) for _ in key.split('_')[1:]]
            LABELS[key] = 'b_{%d,%d}'%(l,m)

        #---

        if args.verbose:
            print('individual draws for : '+name)

        for draw in np.random.choice(np.arange(N-n), replace=False, size=args.num_draws):
            draw += n

            dist.update(**dict((key, samples[key][draw]) for key in KEYS))
            idv[:] = dist.prob(ra, dec) / jac

            hp.mollview(idv, cmap=cmap, min=0.0)
            hp.graticule()

            for ind, key in enumerate(KEYS):
                plt.figtext(0.95, 0.95-0.05*ind, '$%s=%f + 1j*%f$'%(LABELS[key], samples[key][draw].real, samples[key][draw].imag), ha='right', va='top')

            figtmp = os.path.join(
                args.output_dir,
                os.path.basename(__file__)+('-mollweide-%s-%06d%s'%(name, draw, args.tag)) + '.%s'
            )
            for figtype in args.figtype:
                figname = figtmp % figtype
                if args.verbose:
                    print('    saving : '+figname)
                plt.savefig(figname, dpi=args.dpi)
            plt.close()

        #---

        if args.verbose:
            print('average for : '+name)

        ave[:] = 0.0
        for ind, draw in enumerate(np.random.choice(np.arange(N-n), replace=False, size=min(args.num_draws_for_average, N))):
            draw += n

            if args.verbose:
                sys.stdout.write('\r    %d'%ind)
                sys.stdout.flush()

            dist.update(**dict((key, samples[key][draw]) for key in KEYS))
            ave[:] += dist.prob(ra, dec) / jac
        ave /= args.num_draws_for_average

        if args.verbose:
            sys.stdout.write('\n')
            sys.stdout.flush()

        arg = np.argmax(ave)
        print('max --> ra=%.6f deg, dec=%.6f deg'%(ra[arg]*180/np.pi, dec[arg]*180/np.pi))

        hp.mollview(ave, cmap=cmap, min=0.0)
        hp.graticule()

        figtmp = os.path.join(
            args.output_dir,
            os.path.basename(__file__)+('-mollweide-%s-mean%s'%(name, args.tag)) + '.%s'
        )
        for figtype in args.figtype:
            figname = figtmp % figtype
            if args.verbose:
                print('    saving : '+figname)
            plt.savefig(figname, dpi=args.dpi)
        plt.close()
