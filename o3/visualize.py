import numpy as np

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
#plt.rcParams['text.usetex'] = True

from matplotlib.patches import Polygon
from matplotlib import patheffects
from matplotlib import text

#-------------------------------------------------

DEFAULT_DPI = 100
DEFAULT_FIGTYPE = 'png'

#-------------------------------------------------

def close(*args, **kwargs):
    plt.close(*args, **kwargs)

#-------------------------------------------------

def mollweide(
        factors,
        mix,
        color_map='Reds',
        include_edges=True,
        include_vertices=False,
        labels=False,
        grid=True,
        colorbar=True,
        colorbar_label='',
        vmin=None,
        vmax=None,
    ):
    """basic mollweide visualization of factors within different pixels
    """
    fig = plt.figure(figsize=(6,3))
    ax = fig.add_axes([0.06, 0.10, 0.80, 0.80], projection='mollweide')
    if colorbar:
        cb = fig.add_axes([0.87, 0.15, 0.025, 0.70])

    # label axes
    # pre-process data
#    raw = np.array(factors) / np.max(factors)
    raw = np.array(factors)
    if vmax is None:
        vmax = np.max(raw)
    if vmin is None:
        vmin = np.min(raw)

    if vmin < 0:
        vmax = max(vmax, abs(vmin))
        vmin = -vmax

    # iterate and plot
    cmap = matplotlib.cm.get_cmap(color_map)

    for datum, dist in zip(raw, mix.distributions):
        color = cmap((datum-vmin)/(vmax-vmin)) ### should be scaled between 0 and 1

#        for shift in [0, -2*np.pi]:
        for shift in [0, 2*np.pi]:
            if include_edges:
                for (ra, dec), (RA, DEC) in dist.edges:
#                    ax.plot([-ra-shift, -RA-shift], [dec, DEC], color='k', alpha=0.25)
                    ax.plot([ra-shift, RA-shift], [dec, DEC], color='k', alpha=0.25)

            if include_vertices:
                for ra, dec in dist.vertices:
#                    ax.plot(-ra-shift, dec, marker='.', markersize=1, color='k', alpha=1.0)
                    ax.plot(ra-shift, dec, marker='.', markersize=1, color='k', alpha=1.0)

#            verts = np.transpose([-dist.vertices[:,0]-shift, dist.vertices[:,1]])
            verts = np.transpose([dist.vertices[:,0]-shift, dist.vertices[:,1]])
            ax.add_patch(Polygon(verts, closed=True, edgecolor=color, facecolor=color, alpha=1.0))

    ### decorate
    ax.grid(grid, which='both')

    if not labels:
        plt.setp(ax.get_xticklabels(), visible=False)
        plt.setp(ax.get_yticklabels(), visible=False)
#    else:
#        ax.set_xticklabels(['%d$^\circ$'%round(-tick*180/np.pi, 0) for tick in ax.get_xticks()])

    ax.tick_params(
        left=False,
        right=False,
        top=False,
        bottom=False,
        direction='in',
        which='both',
    )

    fig.patch.set_alpha(0.)
    ax.patch.set_alpha(0.)
    ax.set_alpha(0.)

    outline_text(ax)

    # add colorbar
    if colorbar:
        cb.imshow(
            np.vstack(np.linspace(vmin, vmax, 101)),
            extent=[0, 1, vmin, vmax],
            aspect='auto',
            cmap=color_map,
            alpha=1.0,
            origin='lower',
        )

        plt.setp(cb.get_xticklabels(), visible=False)
        cb.yaxis.tick_right()
        cb.yaxis.set_label_position('right')

        cb.set_ylabel(colorbar_label)

        plt.setp(cb.get_yticklabels(), Fontsize=10)

        cb.tick_params(
            left=False,
            right=False,
            top=False,
            bottom=False,
            direction='in',
            which='both',
        )

        cb.patch.set_alpha(0.)
        cb.set_alpha(0.)

        outline_text(cb)

    ### return
    return fig

#------------------------

def outline_text(ax):
    """If we are using a new enough version of matplotlib, then
    add a white outline to all text to make it stand out from the background."""
    effects = [patheffects.withStroke(linewidth=2, foreground='w')]
    for artist in ax.findobj(text.Text):
        artist.set_path_effects(effects)

#-------------------------------------------------

def vectorized(samples, draws=[], levels=[0.50, 0.90]):
    """plot an envelope of the vectorized hyperposterior
    """
    fig = plt.figure(figsize=(6,3))
    ax = fig.add_axes([0.10, 0.10, 0.85, 0.85])

    ### format data
    x = []
    y_mean = []
    y_levels = [[[], []] for _ in levels]
    y_draws = [[] for _ in draws]

    for ind, vect in enumerate(samples):
        x += [ind-0.5, ind+0.5]
        y_mean += [np.mean(vect)]*2

        for lnd, level in enumerate(levels):
            p = 100 * (1.-level) / 2
            y_levels[lnd][0] += [np.percentile(vect, p)]*2
            y_levels[lnd][1] += [np.percentile(vect, 100-p)]*2

        for dnd, draw in enumerate(draws):
            y_draws[dnd] += [vect[draw]]*2

    y_iso = [1./(4*np.pi)]*len(x) ### isotropic rate density

    # now plot
    for lnd in range(len(levels)):
        ax.fill_between(x, y_levels[lnd][0], y_levels[lnd][1], color='orange', alpha=0.25)

    for dnd in range(len(draws)):
        ax.plot(x, y_draws[dnd], color='b', alpha=0.25)

    ax.plot(x, y_mean, color='r', alpha=0.75)

    ax.plot(x, y_iso, color='k', alpha=0.75, linestyle='dashed')

    ### decorate
    ax.set_ylabel('rate density [1/steradian]')
    ax.set_xlabel('constellation')
    plt.setp(ax.get_xticklabels(), visible=False)

    ax.tick_params(
        left=True,
        right=True,
        top=True,
        bottom=True,
        direction='in',
        which='both',
    )

    ax.set_xlim(xmin=x[0], xmax=x[-1])
    ax.set_ylim(ymin=0)

    ### return
    return fig

#-------------------------------------------------

def marginals(prior, posterior):
    """plot histograms of the 1D marginals
    """
    fig = plt.figure(figsize=(4,6))

    ax_prior = plt.subplot(3,1,1)
    ax_posterior = plt.subplot(3,1,2)
    ax_ratio = plt.subplot(3,1,3)

    nbins = max(10, int(0.5*min(len(prior[0]), len(posterior[0]))**0.5))
#    max_x = max(np.max(prior), np.max(posterior))
    max_x = 1.0

    bins = np.linspace(0, max_x, nbins+1)
    alpha = 0.25
    kwargs = dict(bins=bins, histtype='step', density=True, alpha=alpha)

    for prior, posterior in zip(prior, posterior):
        prior, _, _ = ax_prior.hist(prior, color='b', **kwargs)
        posterior, _, _ = ax_posterior.hist(posterior, color='r', **kwargs)

        x = []
        y = []
        for ind in range(nbins):
            x += [bins[ind], bins[ind+1]]
            if prior[ind] > 0:
                y += [posterior[ind] / prior[ind]]*2
            elif posterior[ind] == 0:
                y += [1.0]*2
            else:
                y += [np.infty]*2

        x = np.array(x)
        y = np.array(y)

        truth = (y < np.infty) & (y > 0)

        ax_ratio.plot(x[truth], np.log10(y[truth]), color='m', alpha=alpha)

    ### decorate
    ax_prior.set_ylabel('prior')
    ax_posterior.set_ylabel('posterior')
    ax_ratio.set_ylabel('log10(posterior/prior)')

    for ax in [ax_prior, ax_posterior, ax_ratio]:
        ax.set_xlim(xmin=0, xmax=bins[-1])

        ### add annotation for isotropy
        ylim = ax.get_ylim()
        ax.plot([1/(4*np.pi)]*2, ylim, color='k', linestyle='dashed', alpha=0.75)
        ax.set_ylim(ylim)

        ax.tick_params(
            left=True,
            right=True,
            top=True,
            bottom=True,
            direction='in',
            which='both',
        )

    for ax in [ax_prior, ax_posterior]:
        plt.setp(ax.get_xticklabels(), visible=False)
    ax_ratio.set_xlabel('rate density [1/steradian]')

    plt.subplots_adjust(
        left=0.15,
        right=0.95,
        top=0.95,
        bottom=0.10,
        hspace=0.03,
    )

    # return
    return fig

#-------------------------------------------------

def rate_density_variance(prior_mixing_fracs, posterior_mixing_fracs, areas):
    """estimate the variance of the rate density over the sky
    """
    ### compute variances, assuming fractions are normalized so they sum to 1
    iso = 1./(4*np.pi)
    prior_variance = np.array([np.sum(areas*(fracs/areas - iso)**2) * iso for fracs in np.transpose(prior_mixing_fracs)])**0.5
    prior_variance /= iso ### normalize by isotropic rate density

    posterior_variance = np.array([np.sum(areas*(fracs/areas - iso)**2) * iso for fracs in np.transpose(posterior_mixing_fracs)])**0.5
    posterior_variance /= iso

    ### plot
    fig = plt.figure(figsize=(4,6))

    ax_prior = plt.subplot(3,1,1)
    ax_posterior = plt.subplot(3,1,2)
    ax_ratio = plt.subplot(3,1,3)

    nbins = max(10, int(0.25*min(len(prior_variance), len(posterior_variance))**0.5))
    max_x = max(np.max(prior_variance), np.max(posterior_variance))

    bins = np.linspace(0, max_x, nbins+1)
    alpha = 0.75
    kwargs = dict(bins=bins, histtype='step', density=True, alpha=alpha)

    prior, _, _ = ax_prior.hist(prior_variance, color='b', **kwargs)
    posterior, _, _ = ax_posterior.hist(posterior_variance, color='r', **kwargs)

    x = []
    y = []
    for ind in range(nbins):
        x += [bins[ind], bins[ind+1]]
        if prior[ind] > 0:
            y += [posterior[ind] / prior[ind]]*2
        elif posterior[ind] == 0:
            y += [1.0]*2
        else:
            y += [np.infty]*2

    x = np.array(x)
    y = np.array(y)

    truth = (y < np.infty) & (y > 0)

    ax_ratio.plot(x[truth], np.log10(y[truth]), color='m', alpha=alpha)

    ### decorate
    ax_prior.set_ylabel('prior')
    ax_posterior.set_ylabel('posterior')
    ax_ratio.set_ylabel('log10(posterior/prior)')

    for ax in [ax_prior, ax_posterior, ax_ratio]:
        ax.set_xlim(xmin=0, xmax=bins[-1])

        ax.tick_params(
            left=True,
            right=True,
            top=True,
            bottom=True,
            direction='in',
            which='both',
        )

    for ax in [ax_prior, ax_posterior]:
        plt.setp(ax.get_xticklabels(), visible=False)
    ax_ratio.set_xlabel('$\sigma_{\mathcal{R}} / \left<\mathcal{R}\\right>$')

    plt.subplots_adjust(
        left=0.15,
        right=0.95,
        top=0.95,
        bottom=0.10,
        hspace=0.03,
    )

    ### return    
    return fig

#-------------------------------------------------

def loglikelihood_distributions(posterior_loglike, iso_loglike=None):
    """plot the distributions of the loglikelihood
    """
    if iso_loglike is None:
        label = '$\log\mathcal{L}$'

    else:
        # subtract the reference from the loglikelihood
        if not isinstance(iso_loglike, (int, float)):
            assert len(iso_loglike)==1
            iso_loglike = iso_loglike[0]
        posterior_loglike = posterior_loglike - iso_loglike ### avoid updating in place
        label = '$\Delta\log\mathcal{L}^\mathrm{post}_\mathrm{iso}$'

    ### plot
    fig = plt.figure()
    ax = plt.subplot(1,1,1)

    nbins = max(10, int(0.25*len(posterior_loglike)**0.5))
    min_x = np.min(posterior_loglike)
    max_x = np.max(posterior_loglike)

    bins = np.linspace(min_x, max_x, nbins+1)
    alpha = 0.75
    kwargs = dict(bins=bins, histtype='step', density=True, alpha=alpha)

    ax.hist(posterior_loglike, color='r', **kwargs)

    ### decorate
    ax.set_ylabel('posterior')

    ax.tick_params(
        left=True,
        right=True,
        top=True,
        bottom=True,
        direction='in',
        which='both',
    )

    ax.set_xlabel(label)

    plt.subplots_adjust(
        left=0.10,
        right=0.95,
        top=0.95,
        bottom=0.10,
    )

    ### return    
    return fig
