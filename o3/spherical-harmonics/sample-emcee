#!/usr/bin/env python3

"""a script to sample from the hyperposterior for a rotated hemisphere model
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os

from multiprocessing import (Pool, cpu_count)

from numpy import genfromtxt
import h5py

from argparse import ArgumentParser

### non-standard libraries
from gwdistributions import parse
from gwdistributions import distributions
from gwdistributions.backends import numerics as be
from gwdistributions.backends import names as gwdist_names

import emcee

#-------------------------------------------------

TWOPI = 2.0*be.pi

#-------------------------------------------------

def ndim(max_l):
    ans = 0
    for l in range(max_l+1):
        ans += 1
        for m in range(1, l+1):
            ans += 2
    return ans

def real2complex(samples, max_l):
    ans = dict()
    ind = 0
    for l in range(max_l+1):
        ans['b_%d_0'%l] = samples[:,ind] + 0.0j
        ind += 1

        for m in range(1, l+1):
            real = samples[:,ind]
            ind +=1 

            imag = samples[:,ind]
            ind += 1

            ans['b_%d_%d'%(l,m)] = real + imag*1.0j

    return ans

#-------------------------------------------------

MAX = 1.0
def draw_blm(size=1):
    return MAX*(2*be.random(size=size) - 1)

def draw_from_prior(num_walkers, max_l):
    samples = []
    for l in range(max_l+1):
        samples.append(draw_blm(size=num_walkers)) ### real part
        for m in range(1, l+1):
            samples.append(draw_blm(size=num_walkers)) ### real part
            samples.append(draw_blm(size=num_walkers)) ### imag part
    return be.transpose(be.array(samples))

def logprior(params):
    if be.any(be.abs(params) > MAX):
        return - be.infty
    else:
        return -len(params)*be.log(2*MAX)

#------------------------

def update_model(params, model, max_l):
    ind = 0
    for l in range(max_l+1):
        model['b_%d_0'%l] = params[ind] + 0.0j
        ind += 1
        for m in range(1,l+1):
            real, imag = params[ind:ind+2]
            model['b_%d_%d'%(l,m)] = real + imag*1j
            ind += 2

LOGPDRAW_NAME = 'logpdraw'
def loglikelihood(params, model, max_l, events, injections):
    """the rate-marginalized likelihood
    """
    update_model(params, model, max_l)

    # now compute result
    ans = 0.0

    # iterate over events
    for name, data in events.items():
        ans += be.log(be.sum(model.prob(data) / data[LOGPDRAW_NAME]))

    # compute selection function
    ans += -len(events) * be.log(be.sum(model.prob(injections) / injections[LOGPDRAW_NAME]))

    # return
    return ans

#------------------------

def logposterior_initializer(model, max_l, events, injections):
    """declare these as global variables so they are available within multiprocessing.Pool
    """
    global global_model
    global_model = model
    global global_max_l
    global_max_l = max_l
    global global_events
    global_events = events
    global global_injections
    global_injections = injections

def logposterior(params): #, model, max_l, events, injections):
    """set up posterior with numpyro syntax
    """
    lp = logprior(params)
    ll = loglikelihood(params, global_model, global_max_l, global_events, global_injections)
    return lp+ll, lp, ll ### return blobs!

#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

parser.add_argument('config', type=str)
parser.add_argument('injections', type=str)
parser.add_argument('events', nargs='*', type=str)

parser.add_argument('--max-l', default=1, type=int,
    help='the maximum l allowed in the analysis')

parser.add_argument('--max-far', default=1, type=float)

parser.add_argument('--max-num-injections', default=10000, type=int)
parser.add_argument('--max-num-pe-samples', default=10000, type=int)

parser.add_argument('--num-proc', default=max(1, cpu_count()-1), type=int)
parser.add_argument('--num-walkers', default=10, type=int,
    help='the number of walkers')
parser.add_argument('--num-samples', default=10000, type=int,
    help='the number of posterior samples to draw')

parser.add_argument('-v', '--verbose', default=False, action='store_true')
parser.add_argument('-V', '--Verbose', default=False, action='store_true')
parser.add_argument('--VVerbose', default=False, action='store_true')

parser.add_argument('-o', '--output-dir', default='.', type=str)
parser.add_argument('-t', '--tag', default='', type=str)

args = parser.parse_args()

args.Verbose |= args.VVerbose
args.verbose |= args.Verbose

os.makedirs(args.output_dir, exist_ok=True)

if args.tag:
    args.tag = "_"+args.tag

#-------------------------------------------------

be.use('numpy', verbose=args.VVerbose)
gwdist_names.use('redshift', 'redshift', verbose=args.VVerbose)

#-------------------------------------------------

if args.verbose and (not args.Verbose):
    print('instantiating population model from : '+args.config)

### NOTE: should include mass, redshift, spin, and RADec distributions
model = parse.parse(args.config, verbose=args.Verbose)

if args.verbose:
    print('checking --max-l for consistency')
# check that args.max_l <= biggest l in model
for distribution in model.generators:
    if isinstance(distribution, distributions.SphericalHarmonicRADec):
        # check that we're not specifying max-l that is too big
        assert args.max_l <= distribution._max_l, '--max-l=%d must be <= what is specified within %s (%d)' % \
            (args.max_l, args.config, distribution._max_l)

        if args.verbose:
            print('    --max-l=%d <= dist._max_l=%d'%(args.max_l, distribution._max_l))
            print('    starting spherical harmonics at isotropy')

        # zero all the values
        isotropy_params = dict((name, 0.0j) for name in distribution.param_names)
        isotropy_params['b_0_0'] = 1.0 + 0.0j
        distribution.update(**isotropy_params)

        break ### we did what we needed to do

else:
    raise ValueError('could not find a Spherical Harmonic RADecDistribution')

#-------------------------------------------------

if args.verbose:
    print('loading injections from : '+args.injections)
inj = genfromtxt(args.injections, names=True, delimiter=',')
if args.verbose:
    print('    found %d samples'%len(inj))

inj = inj[inj['far'] <= args.max_far]
if args.verbose:
    print('    found %d samples with FAR <= %.3e, retained %d' % \
        (len(inj), args.max_far, min(len(inj), args.max_num_injections)))
inj = inj[:args.max_num_injections] ### assume random order

inj_dict = dict()

### NOTE: this is hard-coded and a bit fragile...
inj_dict[LOGPDRAW_NAME] = inj['lnprob_declination'] + inj['lnprob_right_ascension'] \
    + inj['lnprob_mass1_source_mass2_source_redshift_spin1x_spin1y_spin1z_spin2x_spin2y_spin2z']
### absolute norm does not matter, and this helps keep numerical scale resonable
inj_dict[LOGPDRAW_NAME] = be.exp(inj_dict[LOGPDRAW_NAME] - be.max(inj_dict[LOGPDRAW_NAME]))

for variate in model.variates:
    inj_dict[variate] = inj[variate]

#------------------------

paths_dict = dict()
event_dicts = dict()
for path in args.events:
    name = os.path.basename(path)[:-7]
    if args.verbose:
        print('loading parameter estimation samples for %s from : %s'%(name, path))
    data = genfromtxt(path, names=True, delimiter=',')
    if args.verbose:
        print('    found %d samples, retained %d'%(len(data), min(len(data), args.max_num_pe_samples)))
    data = data[:args.max_num_pe_samples] ### assume random order

    data_dict = dict()

    ### NOTE: this is hard-coded and a bit fragile...
    data_dict[LOGPDRAW_NAME] = data['lnprob_declination'] + data['lnprob_right_ascension'] \
        + data['lnprob_mass1_source'] + data['lnprob_mass2_source'] + data['lnprob_redshift'] \
        + data['lnprob_spin1x_spin1y_spin1z'] + data['lnprob_spin2x_spin2y_spin2z']
    ### absolute norm does not matter, and this helps keep numerical scale resonable
    data_dict[LOGPDRAW_NAME] = be.exp(data_dict[LOGPDRAW_NAME] - be.max(data_dict[LOGPDRAW_NAME]))

    for variate in model.variates:
        data_dict[variate] = data[variate]

    paths_dict[name] = path ### for storing input arguments
    event_dicts[name] = data_dict ### for actual calculations

#-------------------------------------------------

### run sampler
if args.verbose:
    print('running sampler for prior with %d cores ior %d walkers and %d samples' % \
        (args.num_proc, args.num_walkers, args.num_samples))

if args.num_proc > 1:
    pool = Pool(args.num_proc)
else:
    pool = None

sampler = emcee.EnsembleSampler(
    args.num_walkers,
    ndim(args.max_l),
    logprior,
    pool=pool,
)
sampler.run_mcmc(draw_from_prior(args.num_walkers, args.max_l), args.num_samples, progress=True)

if pool is not None:
    pool.close()

prior_samples = sampler.get_chain(flat=True)

#------------------------

### run sampler
if args.verbose:
    print('running sampler for posterior with %d cores for %d walkers and %d samples' % \
        (args.num_proc, args.num_walkers, args.num_samples))

if args.num_proc > 1:
    pool = Pool(
        args.num_proc,
        initializer=logposterior_initializer,
        initargs=(model, args.max_l, event_dicts, inj_dict),
    )
else:
    pool = None
    logposterior_initializer(model, args.max_l, event_dicts, inj_dict)

sampler = emcee.EnsembleSampler(
    args.num_walkers,
    ndim(args.max_l),
    logposterior,
    pool=pool,
    blobs_dtype=[('logprior', float), ('loglikelihood', float)],
)
sampler.run_mcmc(draw_from_prior(args.num_walkers, args.max_l), args.num_samples, progress=True)

if pool is not None:
    pool.close()

posterior_samples = sampler.get_chain(flat=True)
posterior_blobs = sampler.get_blobs(flat=True)

#------------------------

path = os.path.join(
    args.output_dir,
    os.path.basename(__file__) + args.tag + ".hdf5",
)
if args.verbose:
    print('saving samples to : '+path)

with h5py.File(path, 'w') as obj:

    # store input parameters
    if args.Verbose:
        print('    storing command-line arguments')
    obj.attrs.create('config', args.config)

    obj.attrs.create('max_l', args.max_l)

    obj.attrs.create('injections', args.injections)
    obj.attrs.create('max_far', args.max_far)
    obj.attrs.create('max_num_injections', args.max_num_injections)

    for name, path in paths_dict.items():
        obj.attrs.create(name, path)
    obj.attrs.create('max_num_pe_samples', args.max_num_pe_samples)

    obj.attrs.create('num_walkers', args.num_walkers)
    obj.attrs.create('num_samples', args.num_samples)
    obj.attrs.create('num_proc', args.num_proc)

    #---

    # store prior samples
    if args.Verbose:
        print('    storing prior samples')
    grp = obj.create_group('prior')
    for key, val in real2complex(prior_samples, args.max_l).items():
        grp.create_dataset(key, data=val)

    #---

    # store posterior samples
    if args.Verbose:
        print('    storing posterior samples')
    grp = obj.create_group('posterior')
    for key, val in real2complex(posterior_samples, args.max_l).items():
        grp.create_dataset(key, data=val)
    grp.create_dataset('logprior', data=posterior_blobs['logprior'])
    grp.create_dataset('loglikelihood', data=posterior_blobs['loglikelihood'])

    #---

    # store isotropic model
    if args.Verbose:
        print('    storing isotropic model')
    grp = obj.create_group('isotropic')
    iso_params = [1.0]+[0.0]*(ndim(args.max_l)-1)
    for key, val in real2complex(be.array([iso_params]), args.max_l).items():
        grp.create_dataset(key, data=val)
    grp.create_dataset('logprior', data=logprior(iso_params))
    grp.create_dataset('loglikelihood', data=loglikelihood(iso_params, model, args.max_l, event_dicts, inj_dict))
