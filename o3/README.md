# O3 GW Astrology

This directory contains data based on confident events from O3.

## Single-event Parameter Estimation (PE) samples were taken from

  * O3a : [LIGO-P2000223-v7](https://dcc.ligo.org/public/0169/P2000223/007/all_posterior_samples.tar)
    - two extra events from GWTC-2.1 were provided by Maya Fishbach on 18 April 2022. They were taken from a [zenodo](https://zenodo.org/record/5117703#.Yl3MRXXMKEI) page.
      * IGWN-GWTC2p1-v1-GW190725_174728_PEDataRelease.h5
      * IGWN-GWTC2p1-v1-GW190805_211137_PEDataRelease.h5
  * O3b : [zenodo](https://zenodo.org/record/5546663#.Yl3I5XXMKEI)
    - separate files were downloaded for each event. We grabbed the "nocosmo" versions.

All "default PE priors" are definitely

  * uniform in detector-frame component masses
  * uniform in the cube of the luminosity distance : `p(DL) \propto DL**2`
  * uniform in component spin magnitudes
  * isotropic in component spin direction
  * isotropic over the sky (RA, Dec)
  * uniform in geocenter time
  * isotropic in inclination

I'm less certain that they are

  * uniform in polarization
  * uniform in phase at coalescence

but those probably will not matter.

PE samples are converted to a common format via

  * `./pe-samples/convert`

This reads in only detector-frame parameters of the event and converts to source-frame using our own cosmology (based on Planck's 2018 data).

There is also a list of events that we should exclude from the analysis in

  * `./pe-samples/exclude.txt`

and the set of events included in the analysis is specified in

  * `./pe-samples/event-list.txt`

## Single-event localizations (skymaps) were taken from

  * O3a : [LIGO-P2000223-v7](https://dcc.ligo.org/public/0169/P2000223/007/all_skymaps.tar)
    - we neglect the extra events from GWTC-2.1
  * O3b : [zenodo](https://zenodo.org/record/5546663/files/skymaps.tar.gz?download=1)

These were only used to downselect the "best localized" events.

## Sensitivity estimates were taken from

  * O3(a+b) : [zenodo](https://zenodo.org/record/5546676/files/endo3_mixture-LIGO-T2100113-v12.hdf5?download=1)
