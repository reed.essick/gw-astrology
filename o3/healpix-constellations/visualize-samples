#!/usr/bin/env python

"""a simple script to visualize the data
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os

import h5py
import numpy as np

from argparse import ArgumentParser

### non-standard libraries
from gwdistributions.distributions import HealpixConstellationMixture

import sys
sys.path.insert(0, '..')
import visualize

#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

parser.add_argument('samples', type=str)

parser.add_argument('--nside', default=32, type=int)

parser.add_argument('--seed', default=None, type=int)
parser.add_argument('--num-draws', default=0, type=int,
    help='plot data for this number of random draws from the posterior')

parser.add_argument('-v', '--verbose', default=False, action='store_true')

parser.add_argument('-o', '--output-dir', default='.', type=str)
parser.add_argument('-t', '--tag', default='', type=str)
parser.add_argument('--figtype', default=[], type=str, action='append')
parser.add_argument('--dpi', default=visualize.DEFAULT_DPI, type=int)

args = parser.parse_args()

os.makedirs(args.output_dir, exist_ok=True)

if args.tag:
    args.tag = "_"+args.tag

if not args.figtype:
    args.figtype.append(visualize.DEFAULT_FIGTYPE)

#-------------------------------------------------

if args.seed is not None:
    if args.verbose:
        print('setting numpy.random.seed=%d'%args.seed)
    np.random.seed(args.seed)

#-------------------------------------------------

if args.verbose:
    print('instantiating HealpixConstellationMixture(nside=%d)'%args.nside)
mix = HealpixConstellationMixture(nside=args.nside)

#-------------------------------------------------

if args.verbose:
    print('loading posterior samples from : '+args.samples)

prior = []
posterior = []

isotropic_like = 0.0
posterior_like = 0.0

dist_names = [str(ind) for ind in range(mix.num_components)]
with h5py.File(args.samples, 'r') as obj:
    Nevents = len(obj['event_factors'])
    invareas = obj['invareas'][:]

    for key in dist_names:
        prior.append(obj['prior'][key][:])
        posterior.append(obj['posterior'][key][:])

    for ind in range(Nevents):
        key = 'loglike_%d'%ind
        isotropic_like += obj['isotropic_model'][key][0]
        posterior_like += obj['posterior'][key][:]

    key = 'logpdet'
    isotropic_like += obj['isotropic_model'][key][0] ### already includes the factor of "-Nevents"
    posterior_like += obj['posterior'][key][:]

    iso_attrs = dict(obj['isotropic_model'].attrs.items())
    post_attrs = dict(obj['posterior'].attrs.items())

prior = np.array(prior)
posterior = np.array(posterior)

# normalize mixing fractions
for ind in range(len(prior[0])):
    prior[:,ind] /= np.sum(prior[:,ind])

for ind in range(len(posterior[0])):
    posterior[:,ind] /= np.sum(posterior[:,ind])

#-------------------------------------------------

# choose random posterior samples
if args.verbose:
    print('choosing %d random samples from prior, posterior'%args.num_draws)
prior_draws = np.random.choice(np.arange(len(prior[0])), replace=False, size=args.num_draws)
posterior_draws = np.random.choice(np.arange(len(posterior[0])), replace=False, size=args.num_draws)

#-------------------------------------------------

### MOLLWEIDE PLOTS

exposures = []
iso = 1./(4*np.pi) ### uniform rate density over the sky

# add random draws
kwargs = { 
    'vmin' : 0.0,
    'vmax' : 3.0,
    'color_map' : 'inferno',
    'colorbar_label' : '$\mu_\mathcal{R}/\mathcal{R}_\mathrm{iso}$',
}
for draw in prior_draws:
    exposures.append(('prior-draw-%06d'%draw, \
        kwargs,
        [vect[draw]*invarea/iso for vect, invarea in zip(prior, invareas)]))

for draw in posterior_draws:
    exposures.append(('posterior-draw-%06d'%draw, \
        kwargs,
        [vect[draw]*invarea/iso for vect, invarea in zip(posterior, invareas)]))

# add mean
exposures.append(('prior-mean', \
    kwargs,
    [np.mean(vect)*invarea/iso for vect, invarea in zip(prior, invareas)]))

exposures.append(('posterior-mean', \
    kwargs,
    [np.mean(vect)*invarea/iso for vect, invarea in zip(posterior, invareas)]))

# z-test
kwargs = {
    'vmin' : -1.5,
    'vmax' : +1.5,
    'color_map' : 'viridis',
    'colorbar_label' : '$(\mu_\mathcal{R} - \mathcal{R}_\mathrm{iso})/\sigma_\mathcal{R}$',
}
exposures.append(('prior-z', \
    kwargs,
    [(np.mean(vect)*invarea-iso)/(np.std(vect)*invarea) for vect, invarea in zip(prior, invareas)]))

exposures.append(('posterior-z', \
    kwargs,
    [(np.mean(vect)*invarea-iso)/(np.std(vect)*invarea) for vect, invarea in zip(posterior, invareas)]))

#-------------------------

for name, kwargs, factors in exposures:
    if args.verbose:
        print('plotting mollweide '+name)
    fig = visualize.mollweide(factors, mix, labels=True, **kwargs)
#    fig.suptitle('rate density [1/steradian]')
    figtmp = os.path.join(
        args.output_dir,
        os.path.basename(__file__)+'-mollweide-'+name+args.tag+'.%s'
    )
    for figtype in args.figtype:
        figname = figtmp % figtype
        if args.verbose:
            print('    saving : '+figname)
        fig.savefig(figname, dpi=args.dpi)
    visualize.close(fig)

#-------------------------------------------------

### VECTORIZED POSTERIORS

for name, data, draws in [
        ('prior', prior, prior_draws),
        ('posterior', posterior, posterior_draws),
    ]:
    if args.verbose:
        print('plotting vectorized envelope for '+name)

    fig = visualize.vectorized([vect*invarea for vect, invarea in zip(data, invareas)], draws=draws)

    figtmp = os.path.join(
        args.output_dir,
        os.path.basename(__file__)+'-vectorized-'+name+args.tag+'.%s'
    )
    for figtype in args.figtype:
        figname = figtmp % figtype
        if args.verbose:
            print('    saving : '+figname)
        fig.savefig(figname, dpi=args.dpi)
    visualize.close(fig)

#-------------------------------------------------

### 1D marginal distributions

if args.verbose:
    print('plotting 1D marginal distributions')

fig = visualize.marginals(
    [vect*invarea for vect, invarea in zip(prior, invareas)],
    [vect*invarea for vect, invarea in zip(posterior, invareas)],
)

figtmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+'-marginals'+args.tag+'.%s'
)
for figtype in args.figtype:
    figname = figtmp % figtype
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname, dpi=args.dpi)
visualize.close(fig)

#-------------------------------------------------

### variance of rate density over the sky

if args.verbose:
    print('plottings distribution variance of rate density over the sky')

fig = visualize.rate_density_variance(prior, posterior, 1./invareas)

figtmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+'-rate_variance'+args.tag+'.%s'
)
for figtype in args.figtype:
    figname = figtmp % figtype
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname, dpi=args.dpi)
visualize.close(fig)

#-------------------------------------------------

### distributions of likelihoods

if args.verbose:
    print('plotting distributions of loglikelihoods')

fig = visualize.loglikelihood_distributions(posterior_like, iso_loglike=isotropic_like)

figtmp = os.path.join(
    args.output_dir,
    os.path.basename(__file__)+'-loglikelihood'+args.tag+'.%s'
)
for figtype in args.figtype:
    figname = figtmp % figtype
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname, dpi=args.dpi)
visualize.close(fig)

#-------------------------------------------------

### print a table of information criteria

if args.verbose:
    print('comparing information criteria')

head = '%15s | %15s | %15s | %15s'%('param', 'isotropic', 'anisotropic', 'aniso - iso')
tmp = '%15s | %15.5f | %15.5f | %15.5f'

print(head)
print('-'*len(head))

for key, label in [
        ('num_events', 'Nevents'),
        ('num_params', 'Nparams'),
        ('max_loglike', 'max logL'),
        ('ave_loglike', 'ave logL'),
        ('loglike_ave', 'logL(ave)'),
        ('bayesian_information_criterion', 'BIC'),
        ('akaike_information_criterion', 'AIC'),
        ('deviance_information_criterion', 'DIC'),
    ]:
    print(tmp%(label, iso_attrs[key], post_attrs[key], post_attrs[key]-iso_attrs[key]))
